#pragma once

#include <string>
#include "Sockets.h"
#include "ThreadPool.h"

enum class ClientMessageCodes
{
	Login = 200,
	Update = 204
};

class MultiThreadChatServer;
class Client;
using clientPointer = typename std::shared_ptr<Client>;



class Client
{
public:
	struct ClientUpdateMessage
	{
		ClientMessageCodes Code;
		std::string Source;
		std::string Destination;
		std::string Content;



		ClientUpdateMessage(ClientUpdateMessage&& other) noexcept;
		ClientUpdateMessage(const ClientUpdateMessage& other) = default;
		ClientUpdateMessage(const std::string& source, const std::string& destination, const std::string& content, ClientMessageCodes code) noexcept;

		
		// Whether or not the message has content.
		[[nodiscard]] bool hasContent() const;

		// Whether or not the message is an update request.
		[[nodiscard]] bool isAskForUpdate() const;
		
		// Whether or not the message is a message sending request..
		[[nodiscard]] bool isSendMessage() const;

		// Returns the string representation for a file of the message according to the format.
		[[nodiscard]] std::string toFileRepresentation() const;


		friend std::ostream& operator << (std::ostream& stream, const ClientUpdateMessage& message);
	};

	
	/// <summary>
	/// Initializes a new instance of the <see cref="Client"/> class.
	/// </summary>
	/// <param name="name">The name of the client.</param>
	/// <param name="clientSocketPointer">The shared pointer pointer to the client socket.</param>
	Client(const std::string& name, clientSocketPointer clientSocketPointer);


	/// <summary>
	/// Initializes a new instance of the <see cref="Client"/> class.
	/// </summary>
	/// <param name="other">The other.</param>
	Client(Client&& other) noexcept;

	friend static void MessagesWrapper(int threadID, MultiThreadChatServer* server, const clientPointer client);

	/// <summary>
	/// Receives a client update message from the client in the format of the protocol.
	/// </summary>
	/// <returns>The message constructed in the function</returns>
	ClientUpdateMessage receiveMessage() const;

	/// <summary>
	/// Sends the update message.
	/// </summary>
	/// <param name="otherUsername">The username of the other client.</param>
	/// <param name="fileContent">The content of the file to send.</param>
	/// <param name="allUsers">The map of all of the users connected to the server.</param>
	void sendUpdateMessage(const std::string& otherUsername, const std::string& fileContent, const std::map<std::string, clientPointer>& allUsers) const;
	

private:
	std::string m_Name;
	clientSocketPointer m_Socket;
	ctpl::thread_pool m_ThreadPool;
};

