#include "Sockets.h"

ClientSocket::SocketMessage::operator bool() const
{
	return Worked;
}

int ClientSocket::SocketMessage::toInteger() const
{
	if (Worked) return std::stoi(Message);
	else return 0;
}

ClientSocket::ClientSocket() :
	sf::TcpSocket() {}

ClientSocket::~ClientSocket()
{
	disconnect();
}

bool ClientSocket::connect(const std::string& serverAddress, short serverPort)
{
	return sf::TcpSocket::connect(serverAddress, serverPort) == sf::TcpSocket::Done;
}

ClientSocket::operator bool() const
{
	return isConnected();
}

void ClientSocket::disconnect()
{
	sf::TcpSocket::disconnect();
}

size_t ClientSocket::send(const std::string& message)
{
	size_t bytesSent;

	return (sf::TcpSocket::send(message.c_str(), message.size(), bytesSent) == sf::TcpSocket::Done) ? bytesSent : -1;
}

ClientSocket::SocketMessage ClientSocket::receive(unsigned long long byteAmount)
{
	if (isConnected() && byteAmount > 0)
	{
		size_t bytesReceived;
		std::string stringMessage;
		char* buffer = new char[byteAmount + 1];
		bool worked = sf::TcpSocket::receive(buffer, byteAmount, bytesReceived) == sf::TcpSocket::Done;
		if (bytesReceived)
		{
			buffer[bytesReceived] = NULL;
			stringMessage = static_cast<const char*>(buffer);
		}
		else
		{
			stringMessage = "";
		}
		delete[] buffer;

		return SocketMessage{ worked, stringMessage };
	}
	else
	{
		return { byteAmount > 0, "" };
	}

}

bool ClientSocket::isConnected() const
{
	return sf::TcpSocket::getRemoteAddress() != sf::IpAddress::None;
}

ServerSocket::ServerSocket()
	: m_AlreadyListening(false)
{
}

ServerSocket::ServerSocket(const std::string& listeningAddress)
	: m_IPAddress(listeningAddress), m_AlreadyListening(false)
{
}

ServerSocket::~ServerSocket()
{
	stopListening();
}

bool ServerSocket::listen(unsigned short port)
{
	if (m_AlreadyListening)
	{
		return false;
	}
	else
	{
		m_AlreadyListening = sf::TcpListener::listen(port, m_IPAddress) == sf::TcpListener::Done;
		return m_AlreadyListening;
	}
}

void ServerSocket::stopListening()
{
	if (m_AlreadyListening)
	{
		sf::TcpListener::close();
		m_AlreadyListening = false;
	}
}

clientSocketPointer ServerSocket::accept()
{
	std::shared_ptr<ClientSocket> clientSocket = std::make_shared<ClientSocket>();
	if (sf::TcpListener::accept(*clientSocket) != sf::TcpSocket::Done)
	{
		clientSocket = nullptr;
	}
	return clientSocket;
}

std::string ServerSocket::getIPAddress() const
{
	return m_IPAddress.toString();
}

std::ostream& operator<<(std::ostream& stream, const ClientSocket::SocketMessage& message)
{
	return stream << message.Message;
}
