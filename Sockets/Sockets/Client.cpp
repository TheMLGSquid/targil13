#include "Client.h"
#include <exception>
#include <fstream>
#include <iomanip>
#include <string>
#include <sstream>
#include <iostream>

#define MESSAGE_CODE_LENGTH 3
#define SERVER_UPDATE_MESSAGE_CODE 101
// shortcut
#define MY_ASSERT(expression, exceptionMessage) if (!(expression)) throw std::exception(exceptionMessage)

static std::string createString(unsigned long long number, const int length)
{
	std::ostringstream outputString;
	

	outputString << std::setfill('0') << std::setw(length) << number;

	return outputString.str();
}

Client::ClientUpdateMessage::ClientUpdateMessage(ClientUpdateMessage&& other) noexcept
	: Code(other.Code), Source(std::move(other.Source)), Destination((std::move(other.Destination))), Content(std::move(other.Content))
{
	
}
Client::ClientUpdateMessage::ClientUpdateMessage(const std::string& source, const std::string& destination, const std::string& content, ClientMessageCodes code) noexcept
	: Code(code), Source(source), Destination(destination), Content(content)
{
	
}


bool Client::ClientUpdateMessage::hasContent() const
{
	return !Content.empty();
}

bool Client::ClientUpdateMessage::isAskForUpdate() const
{
	return !Destination.empty() && !hasContent();
}

bool Client::ClientUpdateMessage::isSendMessage() const
{
	return hasContent();
}

std::string Client::ClientUpdateMessage::toFileRepresentation() const
{
	return "&MAGSH_MESSAGE" "&&" "Author&" + Source + "&DATA&" + Content;
}


Client::Client(const std::string& name, clientSocketPointer clientSocketPointer)
	: m_Name(name), m_Socket(clientSocketPointer)
{

}

Client::Client(Client&& other) noexcept
	: m_Name(std::move(other.m_Name)), m_Socket(std::move(other.m_Socket))
{
}


static std::string StringifyAllUsers(const std::map<std::string, clientPointer>& allUsers)
{
	std::string totalString;


	for(const auto&[username, _] : allUsers)
	{
		totalString += username + "&";
	}

	totalString.pop_back();
	return totalString;
}

Client::ClientUpdateMessage Client::receiveMessage() const
{
	ClientMessageCodes messageCode = static_cast<ClientMessageCodes>(m_Socket->receive(MESSAGE_CODE_LENGTH).toInteger());
	MY_ASSERT(messageCode == ClientMessageCodes::Update, "Invalid mid-conversation message");
	
	std::string secondUser = m_Socket->receive(m_Socket->receive(2).toInteger()).Message;
	std::string messageContent = m_Socket->receive(m_Socket->receive(5).toInteger()).Message;
	
	
	return ClientUpdateMessage(m_Name, secondUser, messageContent, messageCode);
}

void Client::sendUpdateMessage(const std::string& otherUsername, const std::string& fileContent, const std::map<std::string, clientPointer>& allUsers) const
{
	std::string allUsernames = StringifyAllUsers(allUsers);
	std::string totalMessage = createString(SERVER_UPDATE_MESSAGE_CODE, 3)
		+ createString(fileContent.size(), 5)
		+ fileContent + createString(otherUsername.size(), 2)
		+ otherUsername
		+ createString(allUsernames.size(), 5)
		+ allUsernames;


	m_Socket->send(totalMessage);
}


