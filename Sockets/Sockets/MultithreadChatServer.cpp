#include "MultithreadChatServer.h"
#include <functional>
#include <string_view>
#include <cstdint>
#include <mutex>
#include <fstream>
#include <iostream>
#include <string>

#include <filesystem>

#define THREAD_AMOUNT 100
#define LOGIN_FIRST_BYTES (5)


// iterate over each value in a queue while emptying it every time to the local variable of name localIteratorName
#define ITERATE_AND_EMPTY_QUEUE(queue, localIteratorName) if(!(queue).empty()) for (const auto& (localIteratorName) = (queue).front(); !(queue).empty(); (queue).pop())



using byte = uint8_t;
using word = uint16_t;


std::ostream& operator<<(std::ostream& stream, const Client::ClientUpdateMessage& message)
{
	return stream << message.toFileRepresentation();
}

/// <summary>
/// A wrapper function for the messages thread function, to match the thread pool requirements.
/// </summary>
/// <param name="threadID">The thread identifier.</param>
/// <param name="server">The pointer to the server object.</param>
/// <param name="client">The shared pointer to the the client object.</param>
static void MessagesWrapper(int threadID, MultiThreadChatServer * server, const clientPointer client)
{
	bool keepListening = true;
	
	while (server->isRunning() && keepListening)
	{
		try
		{
			Client::ClientUpdateMessage message = client->receiveMessage();
			std::string* first; 
			std::string* second;


			if (message.hasContent())
			{
				server->addMessage(message);
			}

			if (message.Source < message.Destination)
			{
				first = &message.Source;
				second = &message.Destination;
			}
			else
			{
				first = &message.Destination;
				second = &message.Source;
			}
			
			client->sendUpdateMessage(message.Destination,
					  server->getFileContent(*first + '&' + *second + ".txt"),
					  server->getUsers());
		
		}
		catch(const std::exception& e)
		{
			LOG("Problem " << e.what());
			keepListening = false;
		}
	}
	server->removeClient(client->m_Name);
	LOG(__FUNCTION__ << "-> Thread of id " << threadID << " has finished running");
}



/// <summary>
/// A wrapper function for the file writing thread function, to match the thread pool requirements.
/// </summary>
static void FileWritingWrapper(int threadID, MultiThreadChatServer* server)
{
	MultiThreadChatServer::writingThread(std::ref(*server));
	LOG(__FUNCTION__ << "-> Thread of id " << threadID << " has finished running");
}

/// <summary>
/// A wrapper function for the connection listening function, to match the thread pool requirements.
/// </summary>
static void ListenThreadWrapper(int threadID, MultiThreadChatServer* server)
{
	MultiThreadChatServer::listenThread(std::ref(*server));
	LOG(__FUNCTION__ << "-> Thread of id " << threadID << " has finished running");
}

/// <summary>
/// Handles the client connection.
/// </summary>
/// <param name="threadID">The thread identifier.</param>
/// <param name="server">The pointer to the server object.</param>
/// <param name="clientSocket">The shared pointer to the client's socket.</param>
static void HandleClientConnection(int threadID, MultiThreadChatServer* server, clientSocketPointer clientSocket)
{
	auto [stillConnected, message] = clientSocket->receive(LOGIN_FIRST_BYTES);

	if (server->m_Running && stillConnected)
	{
		word messageCode = static_cast<word>(std::stol(message.substr(0, 3)));
		byte usernameLength = static_cast<byte>(std::stol(message.substr(3, 2)));
		

		if (usernameLength > 0 && messageCode == 200)
		{
			auto [stillConnected, username] = clientSocket->receive(usernameLength);

			if (stillConnected && server->m_ConnectedUsers.count(username) == 0)
			{
				std::lock_guard<std::mutex> usersLock(server->m_ConnectedUsersMutex);
				
				server->m_ConnectedUsers.emplace(username, std::make_shared<Client>(Client(username, clientSocket)));
				clientPointer client = server->m_ConnectedUsers.find(username)->second;
				client->sendUpdateMessage("", "", server->getUsers());
				server->m_ThreadPool.push(&MessagesWrapper, server, client);
			}
		}
	}
	LOG(__FUNCTION__ << "-> Thread of id " << threadID << " has finished running");
}

std::pair<std::string, unsigned short> parseFileToServerSettings(const std::string& filePath)
{
	std::ifstream file(filePath);
	std::string currentLine;

	std::getline(file, currentLine);
	std::string IPAddress = currentLine.substr(9);

	std::getline(file, currentLine);
	unsigned short port = std::stoi(currentLine.substr(5));
	return { IPAddress, port };
	
}

MultiThreadChatServer::MultiThreadChatServer(const std::string& serverAddress)
	: m_Server(serverAddress), m_ThreadPool(THREAD_AMOUNT)
{

}

MultiThreadChatServer::~MultiThreadChatServer()
{
	m_Server.close();
	
	stopListening();

	for (auto&[fileName, mutex]: m_FileMutexes)
	{
		delete mutex;
	}
	
}

void MultiThreadChatServer::listen(unsigned short port)
{
	m_Running = true;
	m_Server.listen(port);

	m_ThreadPool.push(FileWritingWrapper, this);
	m_ThreadPool.push(ListenThreadWrapper, this);
}

void MultiThreadChatServer::stopListening()
{
	m_Running = false;
	m_WriteToFile.notify_all();
	m_ThreadPool.stop(false);
	m_ThreadPool.clear_queue();
	m_Server.stopListening();
}


bool MultiThreadChatServer::isRunning() const
{
	return m_Running;
}

const std::map<std::string, clientPointer>& MultiThreadChatServer::getUsers() const
{
	return m_ConnectedUsers;
}

std::string MultiThreadChatServer::getFileContent(const std::string& fileName)
{
	std::lock_guard<std::mutex> fileMutexesLock(m_FileMutexesMutex);
	std::ifstream file;
	std::string totalContent;
	
	std::string currentLine;
	
	if(!m_FileMutexes.count(fileName))
	{
		m_FileMutexes.insert({fileName, new std::mutex()});
	}
	
	std::lock_guard<std::mutex> fileLock(*m_FileMutexes.at(fileName));

	file.open(fileName);

	
	if (file.is_open())
	{
		file.seekg(0, std::ios_base::end);
		totalContent.reserve(file.tellg());
		file.seekg(0, std::ios_base::beg);

		while(std::getline(file, currentLine))
		{
			totalContent += currentLine + '\n';
		}
		
		file.close();
	}
	
	return totalContent;
}

void MultiThreadChatServer::removeClient(const std::string& clientName)
{
	std::lock_guard<std::mutex> usersLockGuard(m_ConnectedUsersMutex);
	m_ConnectedUsers.erase(clientName);

}

void MultiThreadChatServer::wait()
{
	std::cin.get();
}

void MultiThreadChatServer::addMessage(const Client::ClientUpdateMessage& message)
{
	if(message.isSendMessage())
	{
		std::lock_guard<std::mutex> messageQueueLock(m_MessageQueueMutex);
		m_MessageQueue.push(message);
		m_WriteToFile.notify_all();
	}
}

void MultiThreadChatServer::listenThread(MultiThreadChatServer& server)
{
	while (server.m_Running)
	{
		clientSocketPointer clientSocket = server.m_Server.accept();

		if (clientSocket) // If the pointer is not null
		{
			// Starts listening for messages from the client and adding it to the connected users list
			// only if the client follows the client protocol and login
			server.m_ThreadPool.push(HandleClientConnection, &server, clientSocket);
			LOG("Accepted user from " << clientSocket->getRemoteAddress());
		}
	}
}

void MultiThreadChatServer::writingThread(MultiThreadChatServer& server)
{
	std::mutex threadMutex;
	while (server.m_Running)
	{
		std::unique_lock<std::mutex> currentThreadLock(threadMutex);

		server.m_WriteToFile.wait(currentThreadLock);
		
		if (server.m_Running && !server.m_MessageQueue.empty())
		{
			std::lock_guard<std::mutex> messagesLock(server.m_MessageQueueMutex);

			ITERATE_AND_EMPTY_QUEUE(server.m_MessageQueue, message)
			{
				std::string filePath = (message.Source < message.Destination)
					                       ? message.Source + "&" + message.Destination + ".txt"
					                       : message.Destination + "&" + message.Source + ".txt";

				LOG("*Writing to " << filePath << "*");
				
				std::ofstream(filePath, std::ios::app) << message.toFileRepresentation();
			}
		}
	}
}



