#pragma once
#include "Client.h"
#include "Sockets.h"
#include "ThreadPool.h"


#include <unordered_map>
#include <map>
#include <condition_variable>
#include <mutex>
#include <atomic>
#include <queue>

#include <tuple>

std::pair<std::string, unsigned short> parseFileToServerSettings(const std::string& filePath);

class MultiThreadChatServer
{
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="MultiThreadChatServer"/> class.
	/// </summary>
	/// <param name="serverAddress">The server address to host on, by default on the localhost.</param>
	explicit MultiThreadChatServer(const std::string& serverAddress = "127.0.0.1");

	
	/// <summary>
	/// Finalizes an instance of the <see cref="MultiThreadChatServer"/> class.
	/// </summary>
	~MultiThreadChatServer();


	/// <summary>
	/// Starts listening on the port and waiting for file writing messages on different threads.
	/// </summary>
	/// <param name="port">The port to listen on.</param>
	void listen(unsigned short port);

	
	/// <summary>
	/// Stops listening for connections and disconnects all of the currently connected users.
	/// </summary>
	void stopListening();

	friend static void ListenThreadWrapper(int threadID, MultiThreadChatServer* server);
	friend static void HandleClientConnection(int threadID, MultiThreadChatServer* server, clientSocketPointer clientSocketPointer);
	friend static void FileWritingWrapper(int threadID, MultiThreadChatServer* server);
	
	/// <summary>
	/// Thread-safely adds the message to the queue of the messages to dispatch.
	/// </summary>
	/// <param name="message">The message to send.</param>
	void addMessage(const Client::ClientUpdateMessage& message);


	/// <summary>
	/// Determines whether this instance is running.
	/// </summary>
	/// <returns>
	///   <c>true</c> if this instance is running; otherwise, <c>false</c>.
	/// </returns>
	bool isRunning() const;


	
	/// <summary>
	/// Returns the map of users connected to the server with their respective sockets.
	/// </summary>
	/// <returns> The user-socket map object </returns>
	const std::map<std::string, clientPointer>& getUsers() const;

	/// <summary>
	/// Returns the full content of the file with the given file path.
	/// </summary>
	/// <param name="fileName">The path to the file to get it's content</param>
	/// <returns>The content of the file.</returns>
	std::string getFileContent(const std::string& fileName);

	/// <summary>
	/// Removes the given client from the client lists.
	/// </summary>
	/// <param name="clientName">Name of the client to remove.</param>
	void removeClient(const std::string& clientName);

	/// <summary>
	/// Waits forever. Literally.
	/// </summary>
	static void wait();

private:
	/// <summary>
	/// The main function for the listening thread.
	/// </summary>
	/// <param name="server">The server reference to the object.</param>
	static void listenThread(MultiThreadChatServer& server);

	/// <summary>
	/// The main function for the file-writing thread.
	/// </summary>
	/// <param name="server">The server reference to the object.</param>
	static void writingThread(MultiThreadChatServer& server);
private:
	ServerSocket m_Server;
	ctpl::thread_pool m_ThreadPool;
	std::map<std::string, clientPointer> m_ConnectedUsers;
	std::atomic_bool m_Running;
	std::queue<Client::ClientUpdateMessage>	m_MessageQueue;
	std::unordered_map<std::string, std::mutex*> m_FileMutexes;


	std::mutex m_FileMutexesMutex;
	std::mutex m_MessageQueueMutex;
	std::mutex m_ConnectedUsersMutex;
	std::condition_variable m_WriteToFile;
};

