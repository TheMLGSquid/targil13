#pragma once
#include <string>
#include <set>
#include "Sockets.h"
#include <tuple>

#define MESSAGE_CODE_HEADER_LENGTH 3


class Message
{
public:



	Message(unsigned int messageCode, const std::string& messageContent);

	std::string getRawText() const;
	std::string getMessageCode() const;


	static Message ServerUpdateMessage(const std::string& messageContent, const std::set<std::string>& users);
	static Message ServerUpdateMessage(const std::set<std::string>& users);
	static Message ReceiveClientMessage(ClientSocket& clientSocket);
	static std::pair<std::string, unsigned int> ConcatenateNames(const std::set<std::string>& names);


private:


	int m_MessageCode;
	std::string m_Content;
};

