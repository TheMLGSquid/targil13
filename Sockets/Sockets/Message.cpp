#if 0
#include "Message.h"
#include <sstream>
#include <iomanip>

using namespace std::string_literals;

Message::Message(unsigned int messageCode, const std::string& messageContent)
	: m_MessageCode(messageCode), m_Content(messageContent)
{
}


static std::string numToString(int number, unsigned int length, bool followedByZeros = true)
{
	std::stringstream streamstream;
	streamstream << std::setw(length) << std::setfill(followedByZeros ? '0' : ' ') << number;
	return streamstream.str();
}

std::string Message::getRawText() const
{
	return getMessageCode() + m_Content;

}

std::string Message::getMessageCode() const
{
	return numToString(m_MessageCode, 3, true);
}

Message Message::ServerUpdateMessage(const std::string& messageContent, const std::set<std::string>& users)
{
	std::string contentLengthString = numToString(messageContent.size(), 5);

}


Message Message::ServerUpdateMessage(const std::set<std::string>& users)
{

	std::string totalString = "";

	for (const std::string& user : users)
	{
		totalString += user + "&";
	}
	totalString.pop_back();

	return Message(101, ("101" "00000" "" "00" "") + numToString(totalString.size(), 5) + totalString);

}

Message Message::ReceiveClientMessage(ClientSocket& clientSocket)
{

}

std::pair<std::string, unsigned int> Message::ConcatenateNames(const std::set<std::string>& names)
{
	return std::pair<std::string, unsigned int>();
}

#endif