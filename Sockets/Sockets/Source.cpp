#include <iostream>

#include "MultithreadChatServer.h"

int main(int argc, char** argv)
{
	if (argc == 2)
	{
		auto[ip, port] = parseFileToServerSettings(argv[1]);
		
		MultiThreadChatServer server(ip);
		
		server.listen(port);
		
		MultiThreadChatServer::wait();
	}
	else
	{
		std::cerr << "Program should be called in the format of Sockets.exe [configFile.txt path]!\n";
		return 1;
	}
}