#pragma once
#include <SFML/Network.hpp>


#include <string>

#include <memory>

#define LOG(expression) (std::cout << expression << '\n')

/// <summary>
/// A class used to represent a TCP connection with a server.
/// </summary>
/// <seealso cref="sf::TcpSocket"/>
class ClientSocket : virtual public sf::TcpSocket
{
public:
	/// <summary>
	/// A struct used to represent a Message received from the server.
	/// </summary>
	struct SocketMessage
	{
		bool Worked;
		std::string Message;

		operator bool() const;

		[[nodiscard]] int toInteger() const;

		friend std::ostream& operator <<(std::ostream& stream, const SocketMessage& message);
	};
	
	/// <summary>
	/// Initializes a new instance of the <see cref="Socket"/> class.
	/// </summary>
	ClientSocket();
	
	/// <summary>
	/// Finalizes an instance of the <see cref="ClientSocket"/> class.
	/// </summary>
	~ClientSocket() override;

	/// <summary>
	/// Connects to the specified server address and port.
	/// </summary>
	/// <param name="serverAddress">The server address.</param>
	/// <param name="serverPort">The server port.</param>
	/// <returns>
	///   <c>true</c> if the connection Worked; otherwise, <c>false</c>.
	/// </returns>
	bool connect(const std::string& serverAddress, short serverPort);


	operator bool() const;

	/// <summary>
	/// Closes the connection.
	/// </summary>
	void disconnect();

	/// <summary>
	/// Sends the specified Message to the other socket.
	/// </summary>
	/// <param name="message">The Message to send.</param>
	/// <returns> The amount of bytes sent, -1 if the sending failed. </returns>
	size_t send(const std::string& message);

	/// <summary>
	/// Receives the specified byte amount from the connection.
	/// </summary>
	/// <param name="byteAmount">The byte amount.</param>
	/// <returns>
	/// The socket Message struct, composed of the Message string and
	/// a bool for whether or not a Message was received.
	/// </returns>
	SocketMessage receive(unsigned long long byteAmount = 1024);

	/// <summary>
	/// Determines whether this instance is connected.
	/// </summary>
	/// <returns>
	///   <c>true</c> if this instance is connected; otherwise, <c>false</c>.
	/// </returns>
	[[nodiscard]] bool isConnected() const;
};

using clientSocketPointer = typename std::shared_ptr<ClientSocket>;

/// A class used to represent a TCP listening server.
class ServerSocket : virtual public sf::TcpListener
{
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="ServerSocket"/> class on the current IP address.
	/// </summary>
	ServerSocket();

	/// <summary>
	/// Initializes a new instance of the <see cref="ServerSocket"/> class.
	/// </summary>
	/// <param name="listeningAddress">The listening address.</param>
	explicit ServerSocket(const std::string& listeningAddress);

	/// <summary>
	/// Finalizes an instance of the <see cref="ServerSocket"/> class.
	/// </summary>
	virtual ~ServerSocket() override;

	/// <summary>
	/// starts listening on the specified port.
	/// </summary>
	/// <param name="port">The port number to listen on.</param>
	/// <returns>Whether or not the listening succeeded.</returns>
	bool listen(unsigned short port);

	/// <summary>
	/// Stops listening on the port given to the listen function.
	/// </summary>
	void stopListening();

	/// <summary>
	/// Accepts a waiting client and creates a connection.
	/// </summary>
	/// <returns>
	/// A shared pointer to the Socket object. No need for delete {ptr_name} at
	/// the end of the scope, works like a normal pointer otherwise.
	/// Returns nullptr if the acceptation failed.
	/// </returns>
	clientSocketPointer accept();

	/// <summary>
	/// Gets the ip address.
	/// </summary>
	/// <returns>the ip address</returns>
	[[nodiscard]] std::string getIPAddress() const;

private:
	sf::IpAddress m_IPAddress;
	bool m_AlreadyListening;
};




